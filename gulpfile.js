{
	"use strict";
	//Module require
	const gulp 		= require("gulp"),
		sass 		= require("gulp-sass"),
		watch 		= require("gulp-watch"),
		babel 		= require("gulp-babel"),
		concat 		= require("gulp-concat"),
		uglify 		= require("gulp-uglify"),
		pug 		= require("gulp-pug"),
        imagemin 	= require('gulp-imagemin'),
		browserSync = require("browser-sync").create(),
		browserify 	= require("gulp-browserify");


	//Files path
	const scssPath 	= "public/src/scss/style.scss",
		allScssPath = "public/src/scss/**/*.scss",
        allJsPath	= "public/src/js/**/*.js",
        vendorJsPath= "public/src/js/plugins/**.js",
        allImgPath	= "public/src/img/**/**",
		allViewPath = "views/**/*.pug",
		viewPath 	= "views/*.pug",
		bundlePath 	= "output/_assets/";

    gulp.task("css", () => {
        gulp.src(scssPath)
            .pipe(sass({outputStyle: "compressed"}))
            .pipe(gulp.dest(bundlePath + "css"))
            .pipe(browserSync.stream({match: '**/*.css'}));
    });

    gulp.task("sass", () => {
        gulp.src(scssPath)
            .pipe(gulp.dest(bundlePath + "scss"))
    });

    gulp.task("img", () => {
        gulp.src(allImgPath)
            .pipe(imagemin())
            .pipe(gulp.dest(bundlePath + "images"));
    });

    gulp.task("vendor", () => {
        gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/owl.carousel/dist/owl.carousel.min.js',
            './node_modules/green-audio-player/dist/js/green-audio-player.min.js',
            './node_modules/infinite-scroll/dist/infinite-scroll.pkgd.min.js',
            './node_modules/smart-app-banner/dist/smart-app-banner.js',
        ], { })
            .pipe(gulp.dest(bundlePath + "js/plugins"));
    });

    gulp.task("js", () => {
        gulp.src(allJsPath)
            .pipe(browserify())
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(bundlePath + "js"));
    });

	gulp.task("view", () => {
		gulp.src(viewPath)
			.pipe(pug({pretty: true}))
			.pipe(gulp.dest("output"));
	});

    gulp.task("fonts", () => {
        gulp.src("public/src/fonts/**/**.**")
            .pipe(gulp.dest(bundlePath + "fonts"));
    });

	gulp.task("browser-sync", () => {
		browserSync.init({
			server: {
				baseDir: "./",
				index: "./output/index.html"
			}
		})
	});

    gulp.task("default", ["browser-sync", "sass", "css", "js", "vendor", "view", "img", "fonts"], () => {
        gulp.watch(allScssPath, ["css"]);
        gulp.watch(vendorJsPath, ["vendor"]);
        gulp.watch(allJsPath, ["js"]);
        gulp.watch(allViewPath, ["view"]);
        gulp.watch(allImgPath, ["img"]);

        gulp.watch(bundlePath + "**/*.js").on('change', browserSync.reload);
        gulp.watch("./output/*.html").on('change', browserSync.reload);
    });

    gulp.task("build", ["css", "sass", "css", "js", "vendor", "view", "img", "fonts"]);
}
