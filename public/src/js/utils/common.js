let modules = {}

modules.mobileMenu = () => {
    $(document).on('click', '.toggle-menu', function () {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
    })
};

modules.homepageSlider = () => {
    $('.homepage-slider').owlCarousel({
        loop: false,
        nav: false,
        dots: true,
        items: 1,
        dotsContainer: '#homepage-custom-dots'
    });
};

modules.carousel3Items = () => {
    $('.carousel-3').each(function (index, item) {
        $(item).owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            margin: 20,
            lazyLoad: true,
            responsiveClass: true,
            dotsContainer: $(item).data('custom-nav-id'),
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });
    });
};

modules.carousel4Items = () => {
    $('.carousel-4').each(function (index, item) {
        $(item).owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            margin: 20,
            lazyLoad: true,
            responsiveClass: true,
            dotsContainer: $(item).data('custom-nav-id'),
            responsive: {
                0: {
                    items: 2
                },
                500: {
                    items: 2
                },
                767: {
                    items: 3
                },
                992: {
                    items: 4
                }
            }
        });
    });

}

modules.customAudioPlay = () => {
    if ($('.audio-player').length > 0)
        new GreenAudioPlayer('.audio-player');
};

modules.mobileMenuFocus = () => {
    var windowWidth = $(window).outerWidth();
    var $mainMenu = $('.header-menu nav');
    var $blogMenu = $('.header-blog-menu nav');
    var $mainMenuActiveItem = $mainMenu.find('.active');
    var $blogMenuActiveItem = $blogMenu.find('.active');
    if (windowWidth < 992 && $blogMenuActiveItem.length > 0) {
        $mainMenu.scrollLeft(($mainMenuActiveItem.position().left - (windowWidth/2)) + ($mainMenuActiveItem.width()/2));
        $blogMenu.scrollLeft(($blogMenuActiveItem.position().left - (windowWidth/2)) + ($blogMenuActiveItem.width()/2) + 14);
    }
}

modules.getNextPost = () => {
    var $articlePageContent = $('.article-page-content');

    if ($articlePageContent.length > 0 && $('.pagination-next').length > 0) {
        $articlePageContent.infiniteScroll({
            path: '.pagination-next',
            append: '.content-wrapper.bg',
            status: '.scroller-status',
            hideNav: '.pagination'
        });
    }
};

modules.progressBar = () => {
    var $articlePageContent = $('.article-page-content');

    if ($articlePageContent.length > 0) {
        $('body').prepend('<div id="progress"></div>');

        $(window).on('load scroll', function () {
            var scrollTop = $(window).scrollTop();
            var bodyHeight = $('body').height();
            var windowHeight = $(window).height();
            $('#progress').css('width', ((scrollTop * 100)/ (bodyHeight - windowHeight) + '%'));
        });
    }

};

modules.fixShareBox = () => {
    var windowWidth = $(window).outerWidth();
    var $shareBox = $('.share-list');

    if (windowWidth > 767 && $shareBox.length > 0) {
        var shareBoxOffsetTop = $shareBox.offset().top;
        var shareBoxTop = $shareBox.position().top;
        var windowHeight = $(window).height();

        $(window).on('load scroll', function () {
            var $articleContentHeight = $('.article-page-content').height();
            var scrollTop = $(window).scrollTop();

            if (scrollTop > shareBoxOffsetTop) {
                if (scrollTop < $articleContentHeight - windowHeight)
                    $shareBox.css('top', scrollTop - shareBoxTop);
            } else {
                $shareBox.removeAttr('style')
            }
        });
    }

}

modules.smartBanner = () => {
    new SmartBanner({
        daysHidden: 1,   // days to hide banner after close button is clicked (defaults to 15)
        daysReminder: 1, // days to hide banner after "VIEW" button is clicked (defaults to 90)
        title: 'Çalık Blog',
        author: 'Çalık Holding',
        button: 'Göster',
        store: {
            ios: 'App Store',
            android: 'Google Play',
        },
        price: {
            ios: 'Ücretsiz',
            android: 'Ücretsiz'
        }
        , theme: 'ios' // put platform type ('ios', 'android', etc.) here to force single theme on all device
        // , icon: '' // full path to icon image if not using website icon image
        // , force: 'ios' // Uncomment for platform emulation
    });
};

modules.toggleClick = () => {
    $('[data-toggle]').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('toggle');
        $(this).toggleClass('active');
        $('[data-content="'+id+'"]').toggleClass('active');
    });
}

module.exports = modules;
