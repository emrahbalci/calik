{

    const common = require('./utils/common.js');

    //mobile menu
    common.mobileMenu();

    //slider
    common.homepageSlider();
    common.carousel3Items();
    common.carousel4Items();

    //custom audio player
    common.customAudioPlay();

    //mobile menu focus
    common.mobileMenuFocus();

    //progress bar
    common.progressBar();

    //infinite post scroll
    common.getNextPost();

    //share box
    common.fixShareBox();

    common.smartBanner();

    common.toggleClick();
}
